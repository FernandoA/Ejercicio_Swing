package Clases;

/**
 * Created by Fernando Ambrosio on 05/07/2017.
 */
public class Producto {

    private int cod_Producto;
    private String nom_Producto;
    private String cliente;
    private float costo;


    public Producto() {
    }

    public Producto(int cod_Producto, String nom_Producto, String cliente, float costo) {
        this.cod_Producto = cod_Producto;
        this.nom_Producto = nom_Producto;
        this.cliente = cliente;
        this.costo = costo;
    }

    public int getCod_Producto() {
        return cod_Producto;
    }

    public void setCod_Producto(int cod_Producto) {
        this.cod_Producto = cod_Producto;
    }

    public String getNom_Producto() {
        return nom_Producto;
    }

    public void setNom_Producto(String nom_Producto) {
        this.nom_Producto = nom_Producto;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }
}
