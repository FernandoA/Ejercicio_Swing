package Clases;

/**
 * Created by Fernando Ambrosio on 04/07/2017.
 */
public class Cliente {

    private int nit;
    private String nombre;
    private String apellido;
    private int edad;

    public Cliente() {
    }

    public Cliente(int nit, String nombre, String apellido, int edad) {
        this.nit = nit;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    public int getNit() {
        return nit;
    }

    public void setNit(int nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}
