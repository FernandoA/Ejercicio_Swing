package Ventanas;

import Clases.Empresa;
import Clases.Producto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Fernando Ambrosio on 11/07/2017.
 */
public class Productov  extends javax.swing.JFrame {

    private JButton agregarButton;
    public JPanel jPanel2;
    private JTable table1;
    private JButton eliminarButton;
    private JButton regresarButton;
    private JTextField codigoText;
    private JTextField descripcionText;
    private JTextField clienteText;
    private JTextField valorText;
    private Empresa empresa;
    private DefaultTableModel model;


    public Productov() {
        empresa = new Empresa();
        model = new DefaultTableModel();
        model.addColumn("Codigo");
        model.addColumn("Descripcion");
        model.addColumn("Cliente");
        model.addColumn("Valor");
        table1.setModel(model);


        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertarProducto();
                for (Producto producto: empresa.getListaProductos()) {
                    model.addRow(new Object[]{producto.getCod_Producto(), producto.getNom_Producto(), producto.getCliente(), producto.getCosto() });
                }
            }
        });
        regresarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                regresar();

            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });
    }

    private void regresar() {
        JFrame jFrame = new JFrame("Inicio");
        jFrame.setContentPane(new Inicio().jPanel);
        jFrame.setMinimumSize(new Dimension(900,600));
        jFrame.setUndecorated(true);
        jFrame.setLocationRelativeTo(null);
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    }

    private void insertarProducto() {
        Producto producto = new Producto();
        producto.setCod_Producto(Integer.parseInt(codigoText.getText()));
        producto.setNom_Producto(descripcionText.getText());
        producto.setCliente(clienteText.getText());
        producto.setCosto(Integer.parseInt(valorText.getText()));
        empresa.insertarProducto(producto);

        codigoText.setText("");
        descripcionText.setText("");
        clienteText.setText("");
        valorText.setText("");
    }
}
