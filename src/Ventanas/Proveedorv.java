package Ventanas;

import Clases.Proveedor;
import Clases.Empresa;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;




/**
 * Created by Fernando Ambrosio on 11/07/2017.
 */
public class Proveedorv extends javax.swing.JFrame {


    private JTable table1;
    public JPanel jPanel, jPanel1;
    private JButton agregarButton;
    private JButton regresarButton;
    private JButton eliminarButton1;
    private JTextField codigotext;
    private JTextField nombretext;
    private JTextField direcciontext;
    private JTextField telefonotext;
    private Empresa empresa;
    private DefaultTableModel model;

    public Proveedorv() {
        empresa = new Empresa();
        model = new DefaultTableModel();
        model.addColumn("Codigo");
        model.addColumn("Nombre");
        model.addColumn("Direccion");
        model.addColumn("Telefono");
        table1.setModel(model);


        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertarProveedor();
                for (Proveedor proveedor: empresa.getListaProveedor()) {
                    model.addRow(new Object[]{proveedor.getCodProveedor(), proveedor.getNom_Proveedor(), proveedor.getDireccion(), proveedor.getTelefono() });
                }
            }
        });





        regresarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                regresar();
            }
        });

        eliminarButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });
    }

    private void regresar() {
        JFrame jFrame = new JFrame("Inicio");
        jFrame.setContentPane(new Inicio().jPanel);
        jFrame.setMinimumSize(new Dimension(900,600));
        jFrame.setUndecorated(true);
        jFrame.setLocationRelativeTo(null);
        jFrame.setVisible(true);
    }

    private void insertarProveedor() {
        Proveedor proveedor = new Proveedor();
        proveedor.setCodProveedor(Integer.parseInt(codigotext.getText()));
        proveedor.setNom_Proveedor(nombretext.getText());
        proveedor.setDireccion(direcciontext.getText());
        proveedor.setTelefono(Integer.parseInt(telefonotext.getText()));
        empresa.insertarProveedor(proveedor);

        codigotext.setText("");
        nombretext.setText("");
        direcciontext.setText("");
        telefonotext.setText("");
    }



}
