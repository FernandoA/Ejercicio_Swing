package Ventanas;

import Clases.Cliente;
import Clases.Empresa;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Fernando Ambrosio on 11/07/2017.
 */
public class Clientev {
    private JButton eliminarButton;
    private JButton agregarButton;
    private JButton regresarButton;
    public JPanel jPanel3;
    private JTable table1;
    private JTextField nitText;
    private JTextField nombreText;
    private JTextField apellidoText;
    private JTextField edadText;
    private Empresa empresa;
    private DefaultTableModel model;


    public Clientev() {
        empresa = new Empresa();
        model = new DefaultTableModel();
        model.addColumn("NIT");
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Edad");
        table1.setModel(model);



        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertarCliente();
                for (Cliente cliente: empresa.getListaCliente()) {
                    model.addRow(new Object[]{cliente.getNit(), cliente.getNombre(), cliente.getApellido(), cliente.getEdad() });
                }
            }
        });
        regresarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                regresar();
            }
        });

        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });
    }


        private void regresar() {
            JFrame jFrame = new JFrame("Inicio");
            jFrame.setContentPane(new Inicio().jPanel);
            jFrame.setMinimumSize(new Dimension(900,600));
            jFrame.setUndecorated(true);
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
        }


    private void insertarCliente() {
        Cliente cliente = new Cliente();
        cliente.setNit(Integer.parseInt(nitText.getText()));
        cliente.setNombre(nombreText.getText());
        cliente.setApellido(apellidoText.getText());
        cliente.setEdad(Integer.parseInt(edadText.getText()));
        empresa.insertarCliente(cliente);

        nitText.setText("");
        nombreText.setText("");
        apellidoText.setText("");
        edadText.setText("");
    }
}
