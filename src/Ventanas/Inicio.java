package Ventanas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Created by Fernando Ambrosio on 10/07/2017.
 */
public class Inicio extends javax.swing.JFrame  {

    private JFrame jFrame;
    public JPanel jPanel, jPanel1, jPanel2, jPanel3;
    private JButton salirButton;
    private JButton productoButton;
    private JButton clienteButton;
    private JButton proveedorButton;


    public Inicio() {




        proveedorButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                irProveedor();
                Inicio inicio = new Inicio();
                inicio.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            }
        });



        clienteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                irCliente();
            }
        });
        productoButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                irProducto();
            }
        });

        salirButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    private void irProveedor() {
        JFrame jFrame = new JFrame("Proveedor");
        jFrame.setContentPane(new Proveedorv().jPanel1);
        jFrame.setMinimumSize(new Dimension(900,600));
        jFrame.setUndecorated(true);
        jFrame.setLocationRelativeTo(null);
        jFrame.setVisible(true);

    }

    private void irProducto() {
        JFrame jFrame = new JFrame("Producto");
        jFrame.setContentPane(new Productov().jPanel2);
        jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        jFrame.setMinimumSize(new Dimension(900,600));
        jFrame.setUndecorated(true);
        jFrame.setLocationRelativeTo(null);
        jFrame.setVisible(true);

    }

    private void irCliente() {
        JFrame jFrame = new JFrame("Cliente");
        jFrame.setContentPane(new Clientev().jPanel3);
        jFrame.setMinimumSize(new Dimension(900,600));
        jFrame.setUndecorated(true);
        jFrame.setLocationRelativeTo(null);
        jFrame.setVisible(true);

    }


    public static void main(String[] args)  {
        JFrame jFrame = new JFrame("Inicio");
        jFrame.setContentPane(new Inicio().jPanel);
        jFrame.setMinimumSize(new Dimension(900,600));
        jFrame.setUndecorated(true);
        jFrame.setLocationRelativeTo(null);
        jFrame.setVisible(true);

    }


}
